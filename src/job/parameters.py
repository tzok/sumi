
class Parameters(object):

    def __init__(self):
        """
        EXECUTABLE
        PRE_EXEC
        POST_EXEC
        ARGUMENTS
        ENVIRONMENT
        WORKING_DIRECTORY
        INTERACTIVE
        INPUT
        OUTPUT
        ERROR
        PROJECT
        FILE_TRANSFER
        CLEANUP
        JOB_START_TIME
        WALL_TIME_LIMIT
        TOTAL_CPU_TIME
        TOTAL_PHYSICAL_MEMORY
        CPU_ARCHITECTURE
        OPERATING_SYSTEM_TYPE
        CANDIDATE_HOSTS
        QUEUE
        SPMD_VARIATION
        TOTAL_CPU_COUNT
        TOTAL_GPU_COUNT
        NUMBER_OF_PROCESSES
        PROCESSES_PER_HOST
        THREADS_PER_PROCESS
        JOB_CONTACT
        NAME
        """
